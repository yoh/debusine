# PYTHONDEVMODE to enable PYTHONASYNCDEBUG=1 and other flags
PYTHON_ENVIRONMENT := PYTHONASYNCDEBUG=1 PYTHONDEBUG=1

# Quick sanity check to run before each commit
check:
	tox -e black -e flake8

# Reformat the code and show changes made by black
black:
	black --line-length 80 --skip-string-normalization --diff --quiet debusine/
	black --line-length 80 --skip-string-normalization debusine/

coverage:
	python3 -m coverage erase
	$(PYTHON_ENVIRONMENT) python3 -m coverage run -p ./manage.py test debusine.db debusine.server debusine.web debusine.project debusine.test
	$(PYTHON_ENVIRONMENT) python3 -m coverage run -p -m unittest discover debusine.artifacts
	$(PYTHON_ENVIRONMENT) python3 -m coverage run -p -m unittest discover debusine.client
	$(PYTHON_ENVIRONMENT) python3 -m coverage run -p -m unittest discover debusine.tasks
	$(PYTHON_ENVIRONMENT) python3 -m coverage run -p -m unittest discover debusine.worker
	$(PYTHON_ENVIRONMENT) python3 -m coverage run -p -m unittest discover debusine.utils
	python3 -m coverage combine
	python3 -m coverage html

.PHONY: check black coverage
