# Tox (http://tox.testrun.org/) is a tool for running tests
# in multiple virtualenvs. This configuration file will run the
# test suite on all supported python versions. To use it, "pip install tox"
# and then run "tox" from this directory.

[tox]
envlist =
    {py39,py310}-{django32}-server-unit-tests,
    {py39,py310}-client-tests,
    flake8,
    black,
# Ensure we have no warnings on last Django LTS
    py39-django32-no-warnings,
# Ensure we don't have missing migration files
    migrations
skipsdist = True
skip_missing_interpreters = True

[main]
django_debusine_packages = debusine.db debusine.server debusine.web debusine.project debusine.test

[testenv]
whitelist_externals =
    xvfb-run
setenv =
  LANG=C
  PYTHONASYNCIODEBUG=1
  PYTHONDEBUG=1
passenv =
  PYTHONDEVMODE
commands =
    server-unit-tests: {envpython} ./manage.py test {[main]django_debusine_packages}
    client-tests: {envpython} -m unittest discover debusine.client
		  {envpython} -m unittest discover debusine.tasks
                  {envpython} -m unittest discover debusine.worker
    no-warnings: {envpython} -W error -W ignore:::flatbuffers.compat: -W 'ignore:Decorator `@unittest_run_loop`' ./manage.py test {[main]django_debusine_packages}
    show-warnings: {envpython} -W all ./manage.py test {[main]django_debusine_packages}
    check: {envpython} ./manage.py check
    migrations: {envpython} ./manage.py makemigrations --check --dry-run
sitepackages = True
deps =
    django32: Django>=3.2,<3.3
    tests: aiohttp
    tests: channels_redis
    tests: djangorestframework
    tests: jsonschema
    tests: python_debian
    tests: PyYAML
    tests: requests
    tests: tabulate

[testenv:flake8]
commands = {envpython} -m flake8 --docstring-convention=all --unused-arguments-ignore-variadic-names --unused-arguments-ignore-stub-functions debusine/ debian/tests/
deps =
    flake8
    flake8-builtins
    flake8-docstrings
    flake8-import-order
    flake8-logging-format
    flake8-rst-docstrings
    flake8-unused-arguments

[testenv:black]
commands = {envpython} -m black --line-length 80 --skip-string-normalization --check --diff debusine/ debian/tests/
deps = black>=22.1

[flake8]
max-complexity = 12
max-line-length = 80
exclude = .git,.ropeproject,.tox,__pycache__,debusine/project/settings/local.py,docs/conf.py,*/migrations/*.py
ignore =
# class attribute shadow a builtin function
    A003,
# function name should be lowercase
    N802,
# line break after/before binary operator
    W504, W503
# sphinx's roles unknown to flake8-rst-docstrings until
# https://github.com/peterjc/flake8-rst-docstrings/issues/7
    RST304,
# rules for flake8-docstrings
# docstring - "Missing docstring in public nested class"
    D106,
# docstring - "Multi-line docstring summary should start at the first line"
    D212,
# docstring - "1 blank line required before class docstring"
    D203,
# whitespace before ':' (conflicts with black)
    E203,
enable-extensions = G
application-import-names = debusine
rst-directives = graphviz
