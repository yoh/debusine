# Copyright 2023 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""URLs related to user (e.g. token handling)."""

from django.urls import path

from debusine.web.views import (
    UserTokenCreateView,
    UserTokenDeleteView,
    UserTokenListView,
    UserTokenUpdateView,
)

app_name = "user"

urlpatterns = [
    path(
        "token/",
        UserTokenListView.as_view(),
        name="token-list",
    ),
    path(
        "token/create/",
        UserTokenCreateView.as_view(),
        name="token-create",
    ),
    path(
        "token/<int:pk>/edit/",
        UserTokenUpdateView.as_view(),
        name="token-edit",
    ),
    path(
        "token/<int:pk>/delete/",
        UserTokenDeleteView.as_view(),
        name="token-delete",
    ),
]
