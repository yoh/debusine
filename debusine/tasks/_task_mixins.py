# Copyright 2022, 2023 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Mixins for the Task class."""

import os
import signal
import subprocess
import tempfile
from contextlib import contextmanager
from pathlib import Path
from typing import BinaryIO, Iterator


class FetchBuildUploadMixin:
    """
    Execute task's self._cmdline() and call self.upload_artifacts(build_dir).

    Task must implement self._cmdline() and self.upload_artifacts(dir:Path)
    in order to use this mixin.
    """

    def _execute(self) -> bool:
        """Call self.build() and task's self.upload_artifacts(build_dir)."""
        with (
            self._temporary_directory() as execute_directory,
            self._temporary_directory() as download_directory,
        ):
            self.fetch(download_directory)

            configured = self.configure_for_build(download_directory)

            build_success = False

            if configured:
                build_success = self.build(execute_directory)

            if build_success:
                build_consistency_errors = self.build_consistency_errors(
                    execute_directory
                )

                if len(build_consistency_errors) > 0:
                    build_success = False
                    build_consistency_file = self.create_debug_log_file(
                        "consistency.log"
                    )
                    build_consistency_file.write(
                        "\n".join(sorted(build_consistency_errors))
                    )
                    build_consistency_file.close()

            self.upload_artifacts(
                execute_directory, build_success=build_success
            )

        return build_success

    def build(self, execute_directory: Path) -> bool:
        """
        Run the command returned by self._cmdline().

        :return: True if the command succeeded.
        """
        cmd = self._cmdline()
        self.logger.info("Executing: %s", " ".join(cmd))
        return self.run_cmd(cmd, execute_directory)

    def configure_for_build(self, download_directory: Path) -> bool:
        """
        Configure Task: set variables needed for the build() step.

        Raise TaskConfigForBuildError() if it cannot be configured.
        """
        raise NotImplementedError()

    @staticmethod
    @contextmanager
    def _temporary_directory() -> Iterator[Path]:
        with tempfile.TemporaryDirectory(
            prefix="debusine-fetch-build-upload-"
        ) as directory:
            yield Path(directory)


class TaskRunCommandMixin:
    """
    Mixin to run commands.

    Use process groups to make sure that the command and possible spawned
    commands are finished and if Task.aborted() is True cancels the
    execution of the command.
    """

    @staticmethod
    def _write_utf8(file: BinaryIO, text: str) -> None:
        file.write(text.encode("utf-8", errors="replace") + b"\n")

    def _write_popen_result(
        self, file: BinaryIO, success: bool, p: subprocess.Popen
    ):
        self._write_utf8(file, f"\naborted: {self.aborted}")
        self._write_utf8(file, f"returncode: {p.returncode}")
        self._write_utf8(file, f"success: {success}")

    def run_cmd(self, cmd: list[str], working_directory: Path) -> bool:
        """
        Execute cmd. If Task.cancelled == True terminates the process.

        Call self.create_debug_log_file("cmd-output.log") and write
        the output of the command (cmd, stdout, stderr and other information)
        in it.

        :param cmd: command to execute with its arguments.
        :param working_directory: working directory where the command
          is executed.
        :return: success.
        """
        cmd_output = self.create_debug_log_file("cmd-output.log", mode="wb")
        cmd_str = [str(arg) for arg in cmd]

        self._write_utf8(cmd_output, f"cmd: {cmd_str}")
        self._write_utf8(cmd_output, "output (contains stdout and stderr):")

        cmd_output.seek(0, os.SEEK_END)

        p = subprocess.Popen(
            cmd,
            start_new_session=True,
            cwd=working_directory,
            stderr=cmd_output.fileno(),
            stdout=cmd_output.fileno(),
        )
        process_group = os.getpgid(p.pid)

        while True:
            if self.aborted:
                break

            try:
                self._wait_popen(p, timeout=1)
                break
            except subprocess.TimeoutExpired:
                pass

        if self.aborted:
            self.logger.debug("Task (cmd: %s PID %s) aborted", cmd, p.pid)
            try:
                if not self._send_signal_pid(p.pid, signal.SIGTERM):
                    # _send_signal_pid failed probably because cmd finished
                    # after aborting and before sending the signal
                    #
                    # p.poll() to read the returncode and avoid leaving cmd
                    # as zombie
                    p.poll()

                    # Kill possible processes launched by cmd
                    self._send_signal_group(process_group, signal.SIGKILL)
                    self.logger.debug("Could not send SIGTERM to %s", p.pid)

                    success = False
                    self._write_popen_result(cmd_output, success, p)
                    cmd_output.close()
                    return success

                # _wait_popen with a timeout=5 to leave 5 seconds of grace
                # for the cmd to finish after sending SIGTERM
                self._wait_popen(p, timeout=5)
            except subprocess.TimeoutExpired:
                # SIGTERM was sent and 5 seconds later cmd
                # was still running. A SIGKILL to the process group will
                # be sent
                self.logger.debug(
                    "Task PID %s not finished after SIGTERM", p.pid
                )
                pass

            # debusine sends a SIGKILL if:
            # - SIGTERM was sent to cmd AND cmd was running 5 seconds later:
            #   SIGTERM was not enough so SIGKILL to the group is needed
            # - SIGTERM was sent to cmd AND cmd finished: SIGKILL to the
            #   group to make sure that there are not processes spawned
            #   by cmd running
            # (note that a cmd could launch processes in a new group
            # could be left running)
            self._send_signal_group(process_group, signal.SIGKILL)
            self.logger.debug("Sent SIGKILL to process group %s", process_group)

            # p.poll() to set p.returncode and avoid leaving cmd
            # as a zombie process.
            # But cmd might be left as a zombie process: if cmd was in a
            # non-interruptable kernel call p.returncode will be None even
            # after p.poll() and it will be left as a zombie process
            # (until debusine worker dies and the zombie is adopted by
            # init and waited on by init). If this happened there we might be a
            # ResourceWarning from Popen.__del__:
            # "subprocess %s is still running"
            #
            # A solution would e to wait (p.waitpid()) that the
            # process finished dying. This is implemented in the unit test
            # to avoid the warning but not implemented here to not delay
            # the possible shut down of debusine worker
            p.poll()
            self.logger.debug("Returncode for PID %s: %s", p.pid, p.returncode)

            success = False
        else:
            # The cmd has finished. The cmd might have spawned
            # other processes. debusine will kill any alive processes.
            #
            # If they existed they should have been finished by cmd:
            # run_cmd() should not leave processes behind.
            #
            # Since the parent died they are adopted by init and on
            # killing them they are not zombie.
            # (cmd might have spawned new processes in a different process
            # group: if this is the case they will be left running)
            self._send_signal_group(process_group, signal.SIGKILL)

            success = p.returncode == 0

        self._write_popen_result(cmd_output, success, p)
        cmd_output.close()
        return success

    def _wait_popen(self, popen: subprocess.Popen, timeout: float):
        return popen.wait(timeout)

    @staticmethod
    def _send_signal_pid(pid, signal) -> bool:
        try:
            os.kill(pid, signal)
        except ProcessLookupError:
            return False

        return True

    @staticmethod
    def _send_signal_group(process_group, signal):
        """Send signal to the process group."""
        try:
            os.killpg(process_group, signal)
        except ProcessLookupError:
            pass
