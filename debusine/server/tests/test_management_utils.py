# Copyright 2022 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Tests for management/utils.py."""

import datetime

from django.test import TestCase

from debusine.server.management import utils


class UtilsTests(TestCase):
    """Tests for methods in management/utils.py."""

    def test_value_or_dash_return_value(self):
        """Assert value_or_dash("something") returns something."""
        self.assertEqual(utils.value_or_dash("something"), "something")

    def test_value_or_dash_return_dash(self):
        """Assert value_or_dash(None) returns "-"."""
        self.assertEqual(utils.value_or_dash(None), "-")

    def test_isoformat_datetime(self):
        """Assert isoformat(dt) returns a formatted datetime."""
        dt = datetime.datetime(2022, 7, 4, 12, 2, 43)
        self.assertEqual(utils.datetime_to_isoformat(dt), "2022-07-04T12:02:43")

    def test_isoformat_dash(self):
        """Assert isoformat(None) returns "-"."""
        self.assertEqual(utils.datetime_to_isoformat(None), "-")
