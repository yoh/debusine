# Copyright 2023 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Utils for the artifacts module."""

from pathlib import Path
from typing import Optional, Type

from debian import deb822 as deb822


def files_in_meta_file_match_files(
    metafile_extension: str,
    metafile_reader: Type[deb822.Deb822],
    files: dict[str, Path],
    ignore_suffixes_metadata: Optional[tuple] = None,
):
    """
    Raise ValidationError if files in the metafile do not match files.

    It searches a file in files with the metafile_extension. Check that
    the files listed in the metafile (a .dsc or .changes) are in files. And
    that no extra files in files.

    :param metafile_extension: for example, .dsc or .changes.
    :param metafile_reader: for example, deb822.Dsc or deb822.Changes.
    :param files: files in the artifact.
    :param ignore_suffixes_metadata: ignore files both in the metadata file
      and in files if they have any of the suffixes.
    """
    if ignore_suffixes_metadata is None:
        ignore_suffixes_metadata = tuple()

    metafile: Optional[Path] = None

    # Find the .dsc file. Is always available because a previous
    # validator verified it.
    for file_name, file_path in files.items():
        if file_name.endswith(metafile_extension):
            metafile = file_path
            break

    with metafile.open() as metafile_obj:
        metafile_files = metafile_reader(metafile_obj)

    files_in_metafile: set[str] = set()

    for file in metafile_files.get("Files", []):
        files_in_metafile.add(file["name"])

    names_in_files = filter(
        lambda f: not f.endswith(metafile_extension), files.keys()
    )
    names_in_files = set(names_in_files)

    if _remove_strings_ending(
        names_in_files, ignore_suffixes_metadata
    ) != _remove_strings_ending(files_in_metafile, ignore_suffixes_metadata):
        raise ValueError(
            f"Files in the package and listed in the {metafile_extension} "
            f"must match (excluding {metafile_extension}). "
            f"Files: {sorted(files.keys())} "
            f"Listed in {metafile_extension}: {sorted(files_in_metafile)}"
        )

    return files


def _remove_strings_ending(input_set: set[str], suffixes: tuple) -> set[str]:
    """
    Return input_set excluding all strings that end in any suffix in "suffixes".

    :param input_set: set of strings to filter
    :param suffixes: tuple of suffixes to exclude

    :return: the filtered set of strings
    """
    return {x for x in input_set if not x.endswith(suffixes)}
