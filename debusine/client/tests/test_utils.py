# Copyright 2023 The Debusine developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Tests for the utils.py methods."""

from unittest import TestCase

import debusine.utils
from debusine.test import TestHelpersMixin


class UtilsTests(TestHelpersMixin, TestCase):
    """Tests for methods in utils.py."""

    def test_find_file_suffixes_multiple_files(self):
        """find_file_suffixes() return two files with different suffixes."""
        temporary_directory = self.create_temporary_directory()

        # Add a deb file
        (deb := temporary_directory / "file.deb").write_text("deb")

        # Add a udeb file
        (udeb := temporary_directory / "file.udeb").write_text("deb")

        # Add a file that is not returned
        (temporary_directory / "README.txt").write_text("README")

        # Check deb and udeb files
        self.assertEqual(
            debusine.utils.find_files_suffixes(
                temporary_directory, [".deb", ".udeb"]
            ),
            sorted([deb, udeb]),
        )

    def test_find_file_suffix_raise_runtime_error(self):
        """find_file() raise RuntimeError: two possible log files."""
        temporary_directory = self.create_temporary_directory()

        suffix = "build"

        (file1 := temporary_directory / f"log1.{suffix}").write_text("Log1")
        (file2 := temporary_directory / f"log2.{suffix}").write_text("Log2")

        files = sorted([str(file1), str(file2)])

        # assertRaisesRegex must have the chars [] escaped
        files = str(files).replace("[", "").replace("]", "")

        with self.assertRaisesRegex(
            RuntimeError, rf"^More than one \['{suffix}'\] file: \[{files}\]$"
        ):
            debusine.utils.find_file_suffixes(temporary_directory, [suffix])

    def test_find_files_suffix(self):
        """find_files_suffix() return multiple files."""
        temporary_directory = self.create_temporary_directory()

        # Create two files to be returned
        (deb1_file := temporary_directory / "hello.deb").write_text("First")
        (deb2_file := temporary_directory / "hello-2.deb").write_text("Second")

        # Create a non-relevant file
        (temporary_directory / "README.txt").write_text("a test")

        # Create a file that could match (*.deb) but is a symbolic link
        (temporary_directory / "temp.deb").symlink_to(deb1_file)

        # Create a file that could match (*.deb) but is a directory
        (temporary_directory / "debian.deb").mkdir()

        self.assertEqual(
            debusine.utils.find_files_suffixes(temporary_directory, [".deb"]),
            sorted([deb1_file, deb2_file]),
        )

    def test_find_files_suffix_include_symbolic_links(self):
        """
        find_files_suffix() return files including symbolic links.

        The option include_symlinks is True.
        """
        temporary_directory = self.create_temporary_directory()

        # Create one file and one symbolic link to the file
        file_name = "file.txt"
        symbolic_link_name = "symbolic.txt"
        (file := temporary_directory / file_name).write_text("test")
        (symbolic_link := temporary_directory / symbolic_link_name).symlink_to(
            file
        )

        self.assertEqual(
            debusine.utils.find_files_suffixes(
                temporary_directory, [".txt"], include_symlinks=True
            ),
            [file, symbolic_link],
        )

    def test_find_file_suffixes_one_file(self):
        """find_file_suffixes() return the expected file."""
        temporary_directory = self.create_temporary_directory()

        # Add a log file
        (log_file := temporary_directory / "log.build").write_text("log")

        # Add a file that is not returned
        (temporary_directory / "README.txt").write_text("README")

        # Check return the correct log file
        self.assertEqual(
            debusine.utils.find_file_suffixes(temporary_directory, [".build"]),
            log_file,
        )
