# Copyright 2022 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Tests for utils module."""

import hashlib
from pathlib import Path
from unittest import TestCase

from debian import deb822

from debusine.test import TestHelpersMixin
from debusine.utils import (
    CALCULATE_HASH_CHUNK_SIZE,
    calculate_hash,
    parse_content_range_header,
    parse_range_header,
    read_dsc,
)


class UtilsTests(TestHelpersMixin, TestCase):
    """Tests for the utils functions."""

    def test_calculate_hash_small_file(self):
        """calculate_hash return the correct sha256 for the file."""
        contents = b"debusine"
        sha256_expected = hashlib.sha256(contents).digest()

        temporary_file = self.create_temporary_file(contents=contents)

        self.assertEqual(
            calculate_hash(Path(temporary_file), "sha256"),
            sha256_expected,
        )

    def test_calculate_hash_big_file(self):
        """
        calculate_hash return the correct sha256 for a big file.

        A big file is a file bigger than CALCULATE_HASH_CHUNK_SIZE.
        """
        size_to_write = int(CALCULATE_HASH_CHUNK_SIZE * 2.5)

        contents = bytearray(size_to_write)

        temporary_file = self.create_temporary_file(contents=contents)

        self.assertEqual(
            calculate_hash(Path(temporary_file), "sha256"),
            hashlib.sha256(contents).digest(),
        )

    def test_parse_range_header_no_header(self):
        """parse_range_header return None: no "Range" header."""
        self.assertIsNone(parse_range_header({}))

    def test_parse_range_header_invalid_header(self):
        """parse_range_header raise ValueError: invalid header."""
        invalid_header = "invalid-header"
        error = f'Invalid Range header: "{invalid_header}"'
        with self.assertRaisesRegex(ValueError, error):
            parse_range_header({"Range": invalid_header})

    def test_parse_range_header(self):
        """parse_range_header return dictionary with start and end."""
        start = 10
        end = 24
        range_header = f"bytes={start}-{end}"
        headers = {"Range": range_header}

        self.assertEqual(
            parse_range_header(headers), {"start": start, "end": end}
        )

    def test_parse_content_range_header_no_header(self):
        """parse_content_range_header return None: no "Content-Range"."""
        self.assertIsNone(parse_content_range_header({}))

    def test_parse_content_range_header_start_end_size(self):
        """parse_content_range_header return dict with integers."""
        params = [
            {
                "header": "bytes 10-20/30",
                "expected": {"start": 10, "end": 20, "size": 30},
            },
            {
                "header": "bytes 10-20/*",
                "expected": {"start": 10, "end": 20, "size": "*"},
            },
            {
                "header": "bytes */30",
                "expected": {"start": "*", "end": None, "size": 30},
            },
            {
                "header": "bytes */*",
                "expected": {"start": "*", "end": None, "size": "*"},
            },
        ]

        for param in params:
            with self.subTest(param):
                headers = {"Content-Range": param["header"]}

                self.assertEqual(
                    parse_content_range_header(headers), param["expected"]
                )

    def test_parse_content_range_header_invalid_header(self):
        """parse_content_range_header raise ValueError: invalid header."""
        invalid_header = "invalid-header"
        error = f'Invalid Content-Range header: "{invalid_header}"'
        with self.assertRaisesRegex(ValueError, error):
            parse_content_range_header({"Content-Range": invalid_header})

    def test_read_dsc_file(self):
        """_read_dsc return a deb822.Dsc object: it was a valid Dsc file."""
        dsc_file = self.create_temporary_file()
        self.write_dsc_example_file(dsc_file)

        dsc = read_dsc(dsc_file)

        self.assertIsInstance(dsc, deb822.Dsc)

    def test_read_dsc_file_invalid(self):
        """_read_dsc return None: it was a not a valid Dsc file."""
        dsc_file = self.create_temporary_file(contents=b"invalid dsc file")

        dsc = read_dsc(dsc_file)

        self.assertIsNone(dsc)
