#!/usr/bin/env python3

# Copyright 2022 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.
"""
Debusine integration tests.

Test sbuild related code.
"""

import argparse
import glob
import logging
import shlex
import sys
import tempfile
import textwrap
import time
import unittest
from pathlib import Path
from typing import Optional

from debian import deb822

import requests

from utils import common
from utils.client import Client
from utils.common import Configuration
from utils.server import DebusineServer
from utils.worker import Worker

from debusine.test import TestHelpersMixin

logger = logging.getLogger(__name__)


class IntegrationTaskSbuildTests(TestHelpersMixin, unittest.TestCase):
    """
    Integration test for the sbuild task.

    These tests assume:
    - debusine-server is running
    - debusine-worker is running (connected to the server)
    - debusine-client is correctly configured
    - "sudo -u debusine-server debusine-admin commands" works
    - schroot "chroot:stable-amd64-sbuild" is set up
    - environment variable AUTOPKGTEST_TMP is set

    debusine-worker is not started (neither restarted) during the tests.
    """

    TASK_NAME = 'sbuild'

    def setUp(self):
        """Initialize test."""
        self._sbuild_changes_file_path: Optional[Path] = None

        # If debusine-server or nginx was launched just before the
        # integration-tests.py is launched the debusine-server might not be
        # yet available. Let's wait for the debusine-server to be
        # reachable if it's not ready yet
        self.assertTrue(
            DebusineServer.wait_for_server_ready(),
            'debusine-server should be available (in '
            f'{Configuration.BASE_URL}) before the integration tests are ran',
        )

        self.distribution = "stable"
        self.architecture = "amd64"

        self.assertTrue(
            self.schroot_available(self.distribution, self.architecture)
        )

        self._worker = Worker()

    def test_work_request_sbuild_from_dsc(self):
        """Create a valid sbuild job, assert output."""
        package = 'hello'
        version = '2.10-2'

        task_data = self.create_task_data_from_dsc(
            self.distribution, self.architecture, package, version
        )

        # Client submits a work-request
        work_request_id = Client.execute_command(
            'create-work-request', self.TASK_NAME, stdin=task_data
        )['work_request_id']

        # The worker should get the new work request and start executing it
        self.assertTrue(
            Client.wait_for_work_request_completed(work_request_id, "success")
        )

        sbuild_changes_absolute_path = self.search_file_in_tmp(
            self.sbuild_changes_file_name().name
        )

        with open(sbuild_changes_absolute_path) as f:
            changes = deb822.Changes(f)

        self.assertDictContainsSubset(
            dict(changes),
            {
                'Distribution': self.distribution,
                'Architecture': self.architecture,
                'Source': package,
                'Version': version,
            },
        )

    def download_url(self, url: str, directory: Path):
        """
        Download URL and save it to the directory.

        The file name is based on the URL file name.
        """
        with open(directory / url.split("/")[-1], "wb") as file:
            file.write(requests.get(url).content)

    def create_artifact_to_build_hello(self) -> int:
        """Create an artifact with the for the hello package."""
        temp_directory = tempfile.TemporaryDirectory(
            prefix="debusine-integration-tests-"
        )

        self.addCleanup(temp_directory.cleanup)

        temp_directory = Path(temp_directory.name)

        self.download_url(
            "http://deb.debian.org/debian/pool/main/h/hello/hello_2.10-2.dsc",
            temp_directory,
        )
        self.download_url(
            "http://deb.debian.org/debian/pool/main/h/hello/hello_2.10.orig.tar.gz",  # noqa: E501
            temp_directory,
        )
        self.download_url(
            "http://deb.debian.org/debian/pool/main/h/hello/hello_2.10-2.debian.tar.xz",  # noqa: E501
            temp_directory,
        )

        files = glob.glob(f"{temp_directory}/*")

        artifact_id = Client.execute_command(
            "create-artifact",
            "integration-test",
            "--workspace",
            "System",
            "--upload",
            *files,
        )["artifact_id"]

        return artifact_id

    def test_work_request_sbuild_from_artifact(self):
        """Create an sbuild job: download the artifact and build."""
        artifact_id = self.create_artifact_to_build_hello()

        task_data = self.create_task_data_from_artifact(
            self.distribution, self.architecture, artifact_id
        )

        # Client submits a work-request
        work_request_id = Client.execute_command(
            "create-work-request", self.TASK_NAME, stdin=task_data
        )["work_request_id"]

        # The worker should get the new work request and start executing it
        self.assertTrue(
            Client.wait_for_work_request_completed(work_request_id, "success")
        )

    def test_create_sbuild_schroot_not_found(self):
        """Create a sbuild job, schroot does not exist. Status is pending."""
        distribution = 'does-not-exist'
        architecture = 'amd64'
        self.assertFalse(self.schroot_available(distribution, architecture))

        task_data = self.create_task_data_from_dsc(
            distribution, architecture, 'hello', '2.10-2'
        )

        # Client submits a work-request
        work_request_id = Client.execute_command(
            'create-work-request', self.TASK_NAME, stdin=task_data
        )['work_request_id']

        # Currently a work request for which there is no an available worker
        # stays in the pending status. Let's wait 5 seconds and check that
        # it is still in pending
        time.sleep(5)

        status = Client.execute_command('show-work-request', work_request_id)

        self.assertEqual(status['status'], 'pending')

        # The file was not created
        self.assertFalse(self.sbuild_changes_file_name().exists())

    @staticmethod
    def schroot_available(distribution: str, architecture: str) -> bool:
        """
        Assert that a specific chroot is available.

        Looks for chroot:{distribution}-{architecture}-sbuild in the output
        of schroot --list

        The integration test does not set up any sbuild but use
        an available one.
        """
        # To set them up see debian/tests/integration-tests-sbuild
        # For example:
        # adduser debusine-worker sbuild
        # sbuild-createchroot stable /srv/chroot/stable)
        output = common.run(['schroot', '--list']).stdout

        expected_line = f'chroot:{distribution}-{architecture}-sbuild'

        return expected_line in output

    def sbuild_changes_file_name(self) -> Path:
        """
        Return path to a non-existing temporary file in /tmp.

        Return the same path for each test. Add cleanup to delete the file.
        """
        if self._sbuild_changes_file_path:
            return self._sbuild_changes_file_path

        self._sbuild_changes_file_path = self.create_temporary_file(
            prefix="sbuild.changes-", directory="/tmp"
        )
        self._sbuild_changes_file_path.unlink()
        return self._sbuild_changes_file_path

    @staticmethod
    def search_file_in_tmp(file_name):
        """Return the absolute file path for file_name in /tmp or None."""
        for file in glob.glob('/tmp/**', recursive=True):
            if file.endswith(file_name):
                return file

        return None

    def create_task_data_from_dsc(
        self, distribution: str, architecture: str, package: str, version: str
    ) -> str:
        """
        Return valid task_data to create an sbuild work request from a .dsc.

        Only packages in debian/pool/main
        """
        changes_path = shlex.quote(self.sbuild_changes_file_name().as_posix())

        url = (
            'https://deb.debian.org/debian/pool/main/'
            f'{package[0]}/{package}/{package}_{version}.dsc'
        )

        return textwrap.dedent(
            f'''\
            build_components:
            - any
            - all
            distribution: {distribution}
            host_architecture: {architecture}
            input:
              source_package_url: {url}
            sbuild_options:
            - --post-build-commands=/bin/cp %SBUILD_CHANGES "{changes_path}"
        '''
        )

    @staticmethod
    def create_task_data_from_artifact(
        distribution: str, architecture: str, artifact_id: int
    ) -> str:
        """Return valid task_data for an sbuild work request from artifact."""
        return textwrap.dedent(
            f'''\
            build_components:
            - any
            - all
            distribution: {distribution}
            host_architecture: {architecture}
            input:
              artifact_id: {artifact_id}
        '''
        )


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Task sbuild integration tests for debusine',
    )

    parser.add_argument(
        '--log-level',
        help='Minimum log level. Overrides log-level (in [General] section) '
        'from config.ini',
        choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'],
        default='INFO',
    )

    debusine_args, unittest_args = parser.parse_known_args()

    logging.basicConfig(level=debusine_args.log_level)

    unittest.main(argv=[sys.argv[0]] + unittest_args)
